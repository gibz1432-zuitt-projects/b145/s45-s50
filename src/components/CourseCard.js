import { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'


export default function CourseCard({courseProp}) {
	// console.log(props)
	// console.log(typeof props)
	console.log(courseProp)

//deconstruct the courseProp into their own variables
const {name, description, price, _id} = courseProp
//Syntax
	// const [getter, setter] = usestate(initialValueOfGetter)
// const [count, setCount] = useState(0)

// const [seats, seatCount] = useState(30)

// function enroll(){

// 	if(seats > 0){
// 	setCount(count + 1)
// 	console.log('Enrollees:' + ' ' + count)
	// if(seats > 0) 
	// seatCount(seats - 1)
	// if(seats == 0)
	// return alert('No more seats')
// 	}

// }

// useEffect(() => {
// 	if(seats === 0){
// 		alert('No more seats available')
// 	}
// },[seats])

	return(
		<Card className="cardHighlight p-3">
			<Card.Body>
				<Card.Title>
					{name}
				</Card.Title>
				<Card.Subtitle>
					Description:
				</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				
				<Card.Subtitle>
					Price:
				</Card.Subtitle>
				<Card.Text>
					{price}	
				</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details
				</Link>
			</Card.Body>
		</Card>
		)
}