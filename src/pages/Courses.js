// import coursesData from '../data/coursesData'
import {useEffect, useState} from 'react'
import CourseCard from '../components/CourseCard'


export default function Courses(){

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('https://floating-brook-34045.herokuapp.com/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)
		setCourses(data.map(course => {
		return (

			<CourseCard key = {course._id} courseProp = {course}/>
			);
		}));

		});

	},[]);


	// console.log(coursesData)
	// console.log(coursesData[0])
	return(
	<>	
		<h1>Courses</h1>
	
			
			{courses}
		
	</>

		)
}