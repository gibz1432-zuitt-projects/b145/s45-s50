import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Button} from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function CourseView(){

	const {user} = useContext(UserContext)

	const history = useNavigate()

	const { courseId } = useParams()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const enroll = (courseId) => {
		fetch('https://floating-brook-34045.herokuapp.com/users/enroll', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Enrolled Successfully",
					icon: "success",
					text: "Thank you for enrolling!"
				})

				history("/courses")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}


	useEffect(() =>{
		console.log(courseId)
		fetch(`https://floating-brook-34045.herokuapp.com/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [courseId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30PM to 9:30PM</Card.Text>
							{ user.id !==null?
								<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
								:
								<Link className="btn btn-danger btn-block">Log In to Enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>



		)
}